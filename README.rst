ProductivitiCloud Share Connect
*******************************

Working with the ``alfresco`` ``2733-alfresco-connector`` branch, merge
request:
https://gitlab.com/kb/alfresco/merge_requests/2

Install
=======

Virtual Environment
-------------------

Windows::

  C:/Path/To/Python -m venv venv-connect
  venv-connect\Scripts\activate

Linux::

  virtualenv --python=python3 venv-connect
  source venv-connect/bin/activate

Linux and Windows::

  pip install -r requirements/local.txt

Configuration
=============

Create a ``config.yaml`` file in the output folder i.e. the same folder as you
write the ``yaml`` and document files:

.. code-block:: yaml

  url: https://www.productiviticloud.com/
  user: my-username
  pass: my-password

``url``

  If the Alfresco URL

``user``

  The Alfresco user name

``pass``

  The Alfresco password

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

Your application should start by writing the document you want copied to a
folder e.g. ``test.txt``.  You should then write a ``yaml`` file containing the
following data e.g:

.. code-block:: yaml

  name: Apple
  folder-id: ad8d62fc-35d3-4dee-95f7-0a3d35acbc91
  node-type: fn:Purchase_Invoices
  properties:
    fn:invoiceNumber: '44'
    fn:supplierName: Farmer

``folder-id``

  To find the ``folder-id``, log into *Share*, click on the folder you want to
  use, under *Share*, *Copy this link to share the current page*, copy the
  Alfresco ID from the end of the URL e.g. for
  https://www.productiviticloud.com/share/page/site/productiviti/folder-details?nodeRef=workspace://SpacesStore/ad8d62fc-35d3-4dee-95f7-0a3d35acbc91
  the Alfresco ID is ``ad8d62fc-35d3-4dee-95f7-0a3d35acbc91``

``name``

  The document name e.g. *Health and Safety Procedures*.

``node-type``

  The node type for the document.  To view the node types (*Custom Types*)
  browse to ``/share/page/console/admin-console/custom-model-manager`` and
  select a *Model*.

``properties``

  The ``properties`` will be copied to the document properties in Alfresco.
  To view the properties, browse to
  ``/share/page/console/admin-console/custom-model-manager``, select a *Model*
  followed by the *Custom Type* containing the properties you want to set.

Run the connector passing in the name of the ``yaml`` file e.g:

.. code-block:: bash

  python productiviticloud_share_connect.py tests/data/testing.yaml

Windows Distribution
====================

To create a Windows distribution, start by deleting the ``dist`` folder::

  rmdir dist /s /q

Create the package (using ``pyinstaller``)::

  pyinstaller productiviticloud_share_connect.spec

.. note:: This creates the ``dist\productiviticloud_share_connect\`` folder
          which contains ``productiviticloud_share_connect.exe`` and all the
          modules required to run the app.
          The ``dist\productiviticloud_share_connect\`` folder can be copied to
          another computer. ``productiviticloud_share_connect.exe`` will run
          the application
          e.g: ``productiviticloud_share_connect.exe c:\data\testing.yaml``

.. note:: To generate the original ``productiviticloud_share_connect.spec``
          file I ran ``pyinstaller productiviticloud_share_connect.py``

To create a Windows installer:

- Open ``ProductivitiCloudShareConnect.iss`` in the *Inno Setup Compiler*
- Click *Build*, *Compile*
- You will find ``ProductivitiCloudShareConnect.exe`` in the ``Output`` folder.

Usage
-----

Set-up the Alfresco URL, user and password in the ``config.yaml`` file (see
above)::

  "c:\Program Files (x86)\ProductivitiCloudShareConnect\productiviticloud_share_connect.exe" c:\data\testing.yaml
