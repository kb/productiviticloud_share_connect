-r base.txt
-e ../../app/alfresco
httmock
pyinstaller==3.3
pytest
pytest-cov
pytest-flakes
pytest-pep8
pytest-sugar
