# -*- encoding: utf-8 -*-
import argparse
import glob
import logging
import os
import yaml

from alfresco.alfresco import Alfresco


logger = logging.getLogger(__name__)


CONFIG_FILE = 'config.yaml'


class ConnectorError(Exception):

    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr('%s, %s' % (self.__class__.__name__, self.value))


def document_file(yaml_file_name):
    result = None
    yaml_path, yaml_extension = os.path.splitext(yaml_file_name)
    search = '{}.*'.format(yaml_path)
    file_list = glob.glob(search)
    if len(file_list) == 2:
        for file_name in file_list:
            path_name, extension = os.path.splitext(file_name)
            if extension.lower() == yaml_extension.lower():
                pass
            else:
                result = file_name
    if not result:
        raise ConnectorError(
            "Cannot find one document file matching '{}'".format(search)
        )
    return result


def load_config(yaml_file_name):
    path, name = os.path.split(yaml_file_name)
    config_file = os.path.join(path, CONFIG_FILE)
    data = yaml.load(open(config_file, 'r'))
    return data


def info(title, message):
    print('{:>18}: {}'.format(title, message))


def yaml_data(file_name):
    return yaml.load(open(file_name, "r"))


def run(yaml_file_name):
    """Copy a document to Alfresco.

    Keyword arguments:
    yaml_file_name -- the path and file name of the YAML file

    """
    info('Alfresco Connector', '')
    info('yaml', yaml_file_name)
    data = yaml_data(yaml_file_name)
    folder_id = data['folder-id']
    info('folder id', folder_id)
    file_name = document_file(yaml_file_name)
    info('document', file_name)
    name = data['name']
    info('name', name)
    node_type = data['node-type']
    info('node type', node_type)
    properties = data.get('properties')
    config = load_config(yaml_file_name)
    alfresco = Alfresco(config['url'], config['user'], config['pass'])
    alfresco.upload(
        file_name,
        folder_id,
        name=name,
        node_type=node_type,
        auto_rename=True,
        properties=properties,
    )
    info('status', 'Complete')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Add a document to Alfresco'
    )
    parser.add_argument(
        dest='yaml_file_name',
        help="path and name of yaml file e.g. 'data/testing.yaml'"
    )
    args = parser.parse_args()
    run(args.yaml_file_name)
