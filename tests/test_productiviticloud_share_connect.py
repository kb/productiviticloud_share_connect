# -*- encoding: utf-8 -*-
import os
import pytest

from httmock import HTTMock, response

from productiviticloud_share_connect import (
    ConnectorError,
    document_file,
    load_config,
    run,
    yaml_data,
)



def _yaml_file_name(name=None):
    if name is None:
        name = 'testing.yaml'
    file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'data',
        name
    )
    return file_name


def mock_response_upload(url, request):
    headers = {'content-type': 'application/json'}
    content = {
        "entry": {
            "id": "1d9122c6-3c6f-4740-a272-6a90d0a50a49",
        }
    }
    return response(201, content, headers)


def test_config_file():
    yaml_file_name = _yaml_file_name()
    assert os.path.exists(yaml_file_name) is True, yaml_file_name
    assert {
        'pass': 'my-pass',
        'url': 'https://www.my.co.uk/',
        'user': 'patrick',
    } == load_config(yaml_file_name)


def test_document_file():
    yaml_file_name = _yaml_file_name()
    assert os.path.exists(yaml_file_name) is True, yaml_file_name
    file_name = document_file(yaml_file_name)
    with open(file_name) as f:
        data = f.read()
    assert 'Test File in Data Folder\n' == data


def test_document_file_does_not_exist():
    yaml_file_name = _yaml_file_name('does-not-exist.yaml')
    assert os.path.exists(yaml_file_name) is False, yaml_file_name
    with pytest.raises(ConnectorError) as e:
        document_file(yaml_file_name)
    assert 'Cannot find one document file matching' in str(e.value)
    assert "does-not-exist.*" in str(e.value)


def test_run():
    file_name = _yaml_file_name()
    with HTTMock(mock_response_upload):
        run(file_name)


def test_yaml_data():
    file_name = _yaml_file_name()
    assert {
        'name': 'Apple',
        'folder-id': 'ad8d62fc-35d3-4dee-95f7-0a3d35acbc91',
        'node-type': 'fn:Purchase_Invoices',
        'properties': {
            'fn:invoiceNumber': '44',
            'fn:supplierName': 'Farmer',
        },
    } == yaml_data(file_name)
