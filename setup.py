from setuptools import setup


setup(
    name='kb_connect_alfresco',
    version='0.1',
    py_modules=['kb_connect_alfresco'],
)
